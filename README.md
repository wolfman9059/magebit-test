# Instructions on how to run the project locally

If you want check this project you need:

1) Install local server (I prefer MAMP, and next steps works with MAMP);
2) After installation, move to MAMP shortcut, press right mouse button and choose --> *Open file location*;

3) Download htdocs zip file, extract that and copy to MAMP directory ( it will replace existed htdocs directory);
4) Press to MAMP shortcut, then press **"Start Sevrer"**;
5) When Apache server and MySQL server launches (green indicators), press **"Open WebStart page"**;
-------------------------------------------------------------------------------------------------------
## Before launch index.php, need import sql database, next steps are:
6) In WebStart page, find Tools and press **phpMyAdmin**;
7) When phpMyAdmin launches press to **Import**;
8) Press **"Browse"** button and find *email-table.sql* (locates in htdocs directory) and press Go button;
-------------------------------------------------------------------------------------------------------
Now you can press in WebStart page *"My Website"*  and will launch index.php file.


!! In htdocs directory have file called sort_data.php, what can't launched (no links to this file in profile.php or index.php).
If you want to see this file in searchbar type this --> **http://localhost:80/sort_data.php"** and you get access to this file.

I  sure, this instructions will helps check this project;

