<?php  
 
 $connect = mysqli_connect('localhost', 'root', 'root', 'email-table');  
 $query = "SELECT * FROM email_data ORDER BY id DESC";  
 $result = mysqli_query($connect, $query);  
 ?>  
 <!DOCTYPE html>  
 <html>  
      <head>  
      	<style type="text/css">
      		#myTable_filter{
      			display: none;
      		}
      	</style>
           <title>Email registration database</title>  
           <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>    
           <script src="../js/db.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.24/datatables.min.css"/>
 
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.24/datatables.min.js"></script>
      </head>  


     <body onload='buttons_arr()'>
      <input type='text' name='valueToSearch' placeholder='Value To Search' id = 'myInput' onkeyup='myFunction()'>
      <br>           
           <div class="container" style="width:700px;" align="center">  
                 
                <div class="table-responsive" id="employee_table">  
                     <table class="display" style="width:100%" border="1px solid" id="myTable">  
                     	<thead>
                          <tr>  
                              <th>ID</th>  
                              <th>Email</th>  
                              <th>Registration date</th>  
                               <th></th> 
                          </tr>  
                      </thead>
                          <?php  
                          while($row = mysqli_fetch_array($result))  
                          {  
                          ?>  
                          <tr>  
                               <td><?php echo $row["ID"]; ?></td>  
                               <td><?php echo $row["Email"]; ?></td>  
                               <td><?php echo $row["Date_reg"]; ?></td>  
                               <td><a href="delete.php?id=<?php echo $row['ID']; ?>">Delete</a></td>
                          </tr>  
                          <?php  
                          }  
                          ?>  
                     </table>  
                </div>  
           </div>  
           <br>  
      </body>  
 </html>  
 <script>  
 $(document).ready(function() {
    $('#myTable').DataTable( {
        "paging":   true,
        "ordering": true,
        "info":     true
    } );
} );  
 </script>  
