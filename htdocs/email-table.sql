-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 15, 2021 at 07:18 PM
-- Server version: 5.7.24
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `email-table`
--

-- --------------------------------------------------------

--
-- Table structure for table `email_data`
--

CREATE TABLE `email_data` (
  `ID` int(255) NOT NULL,
  `Email` varchar(225) NOT NULL,
  `Date_reg` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `email_data`
--

INSERT INTO `email_data` (`ID`, `Email`, `Date_reg`) VALUES
(2, 'dsadsad@gmail.com', '2021-03-07 12:30:39'),
(3, 'arnold@inbox.lv', '2021-03-07 16:53:27'),
(16, 'arnold23@inbox.lv', '2021-03-10 10:32:51'),
(19, 'arnold34@inbox.lv', '2021-03-10 10:36:21'),
(20, 'wolfman9059@yahoo.com', '2021-03-12 00:13:31'),
(21, 'wolfman9059@gmail.com', '2021-03-14 07:14:20'),
(30, 'wolfman9059@gmail.com', '2021-03-14 22:30:56'),
(31, 'dsafsa@humble.com', '2021-03-14 22:31:42');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `email_data`
--
ALTER TABLE `email_data`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `email_data`
--
ALTER TABLE `email_data`
  MODIFY `ID` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
